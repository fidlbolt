# Build the frontend.
FROM node:20 as frontend
WORKDIR /app
# Install dependencies based only on package*.json for better caching.
COPY frontend/package*.json ./
RUN npm ci
COPY frontend ./
RUN npm run build

# Build the backend.
FROM golang:1.23 as backend
WORKDIR /app
# Install dependencies based only on go.mod for better caching.
COPY backend/go.mod ./
RUN go mod download
COPY backend ./
RUN CGO_ENABLED=0 GOOS=linux go build -mod=readonly -v -o server

# Create the final image.
# Use alpine-glibc since C++ binaries like fidlc require glibc.
FROM frolvlad/alpine-glibc:alpine-3.20_glibc-2.34
RUN apk add --no-cache ca-certificates
COPY --from=frontend /app/dist /static
COPY --from=backend /app/server /server
COPY deployment /deployment

# Run the web server on container startup.
# TODO(https://fxbug.dev/42152111): Remove fuchsia/sdk/banjo.
CMD ["/server", \
    "-static", "/static", \
    "-bin", "/deployment/bin", \
    "-etc", "/deployment/etc", \
    "-fidl", "/deployment/fuchsia/sdk/fidl:/deployment/fuchsia/sdk/banjo:/deployment/fuchsia/zircon"]
