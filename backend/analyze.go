// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"sync"
)

// A library is a FIDL library represented by its name (as spelled in its
// "library" declaration, not necessarily matching directory or file names).
type library string

// The zx library describes Zircon syscalls.
const zxLibrary library = "zx"

// TODO(fxbug.dev/90838): In the `analyze` function we assume a library consists
// of all the *.fidl files in its directory. This matches BUILD.gn fidl targets
// for the most part, but it breaks for library zx because //zircon/vdso/zx only
// includes a few of its .fidl files. The rest are used by kazoo for syscall
// definitions, but not in fidlc invocations for building other FIDL libraries.
// The fidlgen backends are not tested with JSON IR arising from including all
// these files, so we must exclude them here to avoid errors.
var zxLibraryFiles = map[string]struct{}{
	"rights.fidl":    {},
	"zx_common.fidl": {},
}

// A libraryMap stores information derived about a group of libraries.
type libraryMap map[library]libraryInfo

type libraryInfo struct {
	// Directory containing the library.
	dir string
	// FIDL files that make up the library (relative to dir, or absolute).
	files []string
	// Direct dependencies (imported by "using" declarations in files).
	deps []library
}

// A libraryCache is a thread-safe, append-only libraryMap.
type libraryCache struct {
	sync.RWMutex
	libs libraryMap
}

func (c *libraryCache) get(lib library) (libraryInfo, bool) {
	c.RLock()
	defer c.RUnlock()
	info, ok := c.libs[lib]
	return info, ok
}

func (c *libraryCache) storeNew(libs libraryMap) {
	// Avoid write contention by skipping the operation if all the libraries in
	// libs are already in the cache.
	if !c.hasNew(libs) {
		return
	}
	c.Lock()
	defer c.Unlock()
	for lib, info := range libs {
		if _, ok := c.libs[lib]; !ok {
			c.libs[lib] = info
		}
	}
}

func (c *libraryCache) hasNew(libs libraryMap) bool {
	c.RLock()
	defer c.RUnlock()
	for lib := range libs {
		if _, ok := c.libs[lib]; !ok {
			return true
		}
	}
	return false
}

// libraryAnalyzer analyzes FIDL files to infer library dependencies. It caches
// information to speed up future analyses.
type libraryAnalyzer struct {
	// Paths in which to search for FIDL library files. Given library foo.bar,
	// the analyzer will check if $P/foo.bar/ exists for each path $P in paths.
	// If one of these exists, it reads all descendent .fidl files.
	paths []string
	// Global cache of library information.
	cache libraryCache
}

func newLibraryAnalyzer(paths []string) *libraryAnalyzer {
	return &libraryAnalyzer{
		paths: paths,
		cache: libraryCache{
			libs: make(libraryMap),
		},
	}
}

const existingLibraryNotice = "" +
	"NOTE: Your file will be compiled with the others in this library. " +
	"If you don't want this behavior, choose a different library name."

// An analysis is the result of regex-parsing "library" and "using" declarations
// in a FIDL file and all its transitive dependencies that can be located.
type analysis struct {
	// The library name from the input FIDL file, or "" if parsing failed.
	root library
	// Map containing the root library (even when it is "") and all transitive
	// dependencies discovered during analysis.
	libs libraryMap
	// Annotations on the "library" and "using" declarations of the input FIDL
	// file indicating their inferred location in the Fuchsia source tree.
	annotations []annotation
}

// analyze analyzes content. It assumes that filename is a FIDL file containing
// content. If any library cannot be located in a.paths, it is silently ignored.
func (a *libraryAnalyzer) analyze(filename, content string) analysis {
	result := analysis{libs: make(libraryMap)}
	root, err := parseFidl(content)
	if err != nil {
		// If we can't parse the library name, there's no point in continuing.
		// We could make this function return an error and report that to the
		// user. However, we prefer to invoke fidlc anyway and show its error.
		// So, we set the libraryMap such that fidlcFileArgs() will produce
		// --files filename.
		result.libs[""] = libraryInfo{files: []string{filename}}
		return result
	}
	result.root = root.Decl.Library

	// Start with a stack containing the root library (if it's a platform source
	// library, meaning we can expect to find it in the tree) and its imports.
	var stack []library
	if result.root.isPlatform() {
		stack = append(stack, result.root)
	}
	for _, using := range root.Using {
		stack = append(stack, using.Library)
	}

	// Explore all dependencies via depth-first search.
	for len(stack) != 0 {
		// Pop a library and check if it needs processing.
		var lib library
		lib, stack = stack[len(stack)-1], stack[:len(stack)-1]
		if _, ok := result.libs[lib]; ok {
			continue
		}

		// Check the analyzer's cache.
		if info, ok := a.cache.get(lib); ok {
			result.libs[lib] = info
			stack = append(stack, info.deps...)
			continue
		}

		// Find the library's files and dependencies.
		var info libraryInfo
		for _, path := range a.paths {
			for _, dir := range lib.possibleDirs() {
				if !info.empty() {
					break
				}
				info.dir = filepath.Join(path, dir)
				// Keep track of already-seen imports to ensure that info.deps
				// has no duplicates (there won't be duplicates in a valid FIDL
				// file, but separate files might import the same library).
				seen := map[library]struct{}{}
				// Visit all .fidl files, including in subdirectories:
				filepath.Walk(info.dir, func(file string, fi os.FileInfo, err error) error {
					if err != nil || fi.IsDir() || filepath.Ext(file) != ".fidl" {
						return err
					}
					if lib == zxLibrary {
						if _, ok := zxLibraryFiles[fi.Name()]; !ok {
							return nil
						}
					}
					rel, err := filepath.Rel(info.dir, file)
					if err != nil {
						return err
					}
					b, err := ioutil.ReadFile(file)
					if err != nil {
						return err
					}
					parsed, err := parseFidl(string(b))
					if err != nil || parsed.Decl.Library != lib {
						return nil
					}
					info.files = append(info.files, rel)
					for _, using := range parsed.Using {
						dep := using.Library
						if _, ok := seen[dep]; ok {
							continue
						}
						seen[dep] = struct{}{}
						info.deps = append(info.deps, dep)
						stack = append(stack, dep)
					}
					return nil
				})
			}
		}
		if !info.empty() {
			result.libs[lib] = info
		}
	}

	// Cache reusable results first, before adding request-specific information.
	a.cache.storeNew(result.libs)

	// The following handles two cases:
	//   1. (More common) The user typed something like `library foo;`. This
	//      is a new library outside the source tree, consisting of one file.
	//   2. The user typed something like `library fuchsia.io;`. This behaves as
	//      if they were adding a new file to the fuchsia.io library, alongside
	//      the other ones in the source tree.
	rootInfo := libraryInfo{files: []string{filename}}
	for _, using := range root.Using {
		rootInfo.deps = append(rootInfo.deps, using.Library)
		if info, ok := result.libs[using.Library]; ok {
			text := fmt.Sprintf("Found %s in %s", using.Library, info.userVisibleDir())
			result.annotations = append(result.annotations, using.annotation(Info, text))
		}
	}
	if info, ok := result.libs[result.root]; ok {
		rootInfo = mergeInfo(rootInfo, info)
		text := fmt.Sprintf("Found %s in %s\n\n%s",
			result.root, info.userVisibleDir(), existingLibraryNotice)
		result.annotations = append(result.annotations, root.Decl.annotation(Info, text))
	}
	result.libs[result.root] = rootInfo

	return result
}

// fidlcFileArgs returns a list of arguments to pass to fidlc to compile the
// libraries in m. This is a fairly expensive operation. It returns an error if
// there is an import cycle.
func (m libraryMap) fidlcFileArgs() ([]string, error) {
	sorted, err := m.topologicalSort()
	if err != nil {
		return nil, err
	}

	// Iterate in reverse order so that dependencies come before dependents.
	var args []string
	for i := len(sorted) - 1; i >= 0; i-- {
		lib := sorted[i]
		args = append(args, "--files")
		info := m[lib]
		for _, file := range info.files {
			// When the user declares their file as belong to an existing
			// library, their file will be an absolute path in a temp directory.
			// Otherwise, all files are relative to info.dir.
			if filepath.IsAbs(file) {
				args = append(args, file)
			} else {
				args = append(args, filepath.Join(info.dir, file))
			}
		}
	}
	return args, nil
}

// dependencyGraph returns a map representing the the dependency graph of m.
// Each key is a library name, and each value is another map whose keys are its
// direct dependencies.
func (m libraryMap) dependencyGraph() (map[string]interface{}, error) {
	// Make sure there are no cycles, otherwise this will never terminate.
	if _, err := m.topologicalSort(); err != nil {
		return nil, err
	}

	sources := map[library]struct{}{}
	for lib := range m {
		sources[lib] = struct{}{}
	}
	for _, info := range m {
		for _, dep := range info.deps {
			delete(sources, dep)
		}
	}
	var start []library
	for lib := range sources {
		start = append(start, lib)
	}

	var gen func(libs []library) map[string]interface{}
	gen = func(libs []library) map[string]interface{} {
		g := map[string]interface{}{}
		for _, lib := range libs {
			g[string(lib)] = gen(m[library(lib)].deps)
		}
		return g
	}
	return gen(start), nil
}

// topologicalSort runs topological sort on m, whose deps fields define a
// directed graph in adjacency list form. In the resulting list, libraries only
// depend on other libraries later in the list, not on earlier ones. Returns
// an error if there is a cycle.
func (m libraryMap) topologicalSort() ([]library, error) {
	var sorted []library
	incoming := map[library]int{}
	for u := range m {
		incoming[u] = 0
	}
	for _, info := range m {
		for _, v := range info.deps {
			incoming[v]++
		}
	}
	var src []library
	for u, deg := range incoming {
		if deg == 0 {
			src = append(src, u)
		}
	}
	for len(src) != 0 {
		var u library
		u, src = src[len(src)-1], src[:len(src)-1]
		sorted = append(sorted, u)
		for _, v := range m[u].deps {
			incoming[v]--
			if incoming[v] == 0 {
				src = append(src, v)
			}
		}
	}
	for _, deg := range incoming {
		if deg != 0 {
			return nil, errors.New("detected an import cycle")
		}
	}
	return sorted, nil
}

// possibleDirs returns directory names that lib's files might be found in.
func (lib library) possibleDirs() []string {
	if lib == zxLibrary {
		// The zx library is a special case: its .fidl files are in a
		// directory named "vsdo" (fuchsia/zircon/vdso), not "zx".
		return []string{"vdso"}
	}
	// Currently this function always returns one result. It used to also return
	// strings.ReplaceAll(string(lib), ".", "-") back when zircon/system/fidl/
	// had FIDL files under that scheme. We are keeping the []string return type
	// in case this needs to return multiple results again in the future.
	return []string{string(lib)}
}

// isPlatform returns true if lib adheres to the naming scheme for platform
// source libraries (enforced by the FIDL linter).
func (lib library) isPlatform() bool {
	s := string(lib)
	return strings.HasPrefix(s, "fidl.") ||
		strings.HasPrefix(s, "fuchsia.") ||
		strings.HasPrefix(s, "test.")
}

// empty returns true if i represents an empty library (has no files).
func (i *libraryInfo) empty() bool {
	return len(i.files) == 0
}

// userVisibleDir returns a representation of i.dir to show to the user.
func (i *libraryInfo) userVisibleDir() string {
	dir := filepath.ToSlash(i.dir)
	if i := strings.Index(dir, "/fuchsia/"); i != -1 {
		return dir[i+1:]
	}
	return dir
}

// mergeInfo returns a combination of the information in info and other. It
// assumes that only one of them has dir set.
func mergeInfo(info, other libraryInfo) libraryInfo {
	if info.dir != "" && other.dir != "" {
		panic("mergeInfo called with incompatible dirs")
	}
	if info.dir == "" {
		info.dir = other.dir
	}
	info.files = append(info.files, other.files...)
	deps := map[library]struct{}{}
	for _, lib := range info.deps {
		deps[lib] = struct{}{}
	}
	for _, lib := range other.deps {
		if _, ok := deps[lib]; !ok {
			info.deps = append(info.deps, lib)
		}
	}
	return info
}
