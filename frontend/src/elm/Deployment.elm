-- Copyright 2020 The Fuchsia Authors. All rights reserved.
-- Use of this source code is governed by a BSD-style license that can be
-- found in the LICENSE file.


module Deployment exposing
    ( Model
    , Msg
    , checkTime
    , init
    , initCmd
    , subscriptions
    , update
    , view
    )

{-| This module produces a view describing the fidlbolt server deployment.

This merits its own module because the deployment information is dynamic, and
can change from one request to another.

-}

import DateFormat
import DateFormat.Relative as Relative
import Html exposing (Html)
import Html.Attributes as Attributes
import Json.Decode as Decode
import Ports
import Task
import Time



------------- MODEL ------------------------------------------------------------


type alias Model =
    { deployment : Maybe Deployment
    , now : Maybe Time.Posix
    }


type alias Deployment =
    { fidlbolt : Commit
    , fuchsia : Commit
    , rustfmt : Version
    }


type alias Commit =
    { hash : String
    , timestamp : Time.Posix
    }


type alias Version =
    { version : String
    }


init : Model
init =
    { deployment = Nothing
    , now = Nothing
    }


initCmd : Cmd Msg
initCmd =
    checkTime



------------- UPDATE -----------------------------------------------------------


type Msg
    = NoOp
    | SetDeployment Deployment
    | SetNow Time.Posix


update : Msg -> Model -> Model
update msg model =
    case msg of
        NoOp ->
            model

        SetDeployment deployment ->
            { model | deployment = Just deployment }

        SetNow now ->
            { model | now = Just now }


checkTime : Cmd Msg
checkTime =
    Task.perform SetNow Time.now



------------- SUBSCRIPTIONS ----------------------------------------------------


subscriptions : Sub Msg
subscriptions =
    Ports.deploymentUpdated NoOp (Decode.map SetDeployment decodeDeployment)



------------- VIEW -------------------------------------------------------------


view : Model -> Html msg
view model =
    case ( model.deployment, model.now ) of
        ( Just deployment, Just now ) ->
            deploymentView deployment now

        _ ->
            emptyView


emptyView : Html msg
emptyView =
    Html.div []
        [ Html.p []
            [ Html.text "(No deployment information)" ]
        ]


deploymentView : Deployment -> Time.Posix -> Html msg
deploymentView deployment now =
    let
        commitView name makeUrl commit =
            [ Html.text name
            , Html.text " commit "
            , Html.a [ Attributes.href (makeUrl commit) ]
                [ Html.text (String.left 12 commit.hash) ]
            , Html.text " ("
            , timestampView now commit.timestamp
            , Html.text ")"
            ]
    in
    Html.div []
        [ Html.p []
            [ Html.text "This instance of fidlbolt was built with:" ]
        , Html.ul [ Attributes.class "close-list" ]
            [ Html.li [] (commitView "fidlbolt" fidlboltUrl deployment.fidlbolt)
            , Html.li [] (commitView "fuchsia" fuchsiaUrl deployment.fuchsia)
            , Html.li [] (versionView "rustfmt" rustfmtUrl deployment.rustfmt)
            ]
        ]


versionView : String -> (Version -> String) -> Version -> List (Html msg)
versionView name makeUrl version =
    [ Html.text name
    , Html.text " version "
    , Html.a [ Attributes.href (makeUrl version) ]
        [ Html.text version.version ]
    ]


timestampView : Time.Posix -> Time.Posix -> Html msg
timestampView now timestamp =
    Html.span
        [ Attributes.title (formatUtc timestamp) ]
        [ Html.text (Relative.relativeTime now timestamp) ]


formatUtc : Time.Posix -> String
formatUtc =
    DateFormat.format
        [ DateFormat.monthNameAbbreviated
        , DateFormat.text " "
        , DateFormat.dayOfMonthNumber
        , DateFormat.text ", "
        , DateFormat.yearNumber
        , DateFormat.text ", "
        , DateFormat.hourNumber
        , DateFormat.text ":"
        , DateFormat.minuteFixed
        , DateFormat.text " "
        , DateFormat.amPmUppercase
        , DateFormat.text " UTC"
        ]
        Time.utc


fidlboltUrl : Commit -> String
fidlboltUrl commit =
    "http://fuchsia.googlesource.com/fidlbolt/+/" ++ commit.hash


fuchsiaUrl : Commit -> String
fuchsiaUrl commit =
    "http://fuchsia.googlesource.com/fuchsia/+/" ++ commit.hash


rustfmtUrl : Version -> String
rustfmtUrl version =
    "https://github.com/rust-lang/rustfmt/releases/tag/v" ++ version.version



------------- ENCODE / DECODE --------------------------------------------------


decodeDeployment : Decode.Decoder Deployment
decodeDeployment =
    Decode.map3 Deployment
        (Decode.field "fidlbolt" decodeCommit)
        (Decode.field "fuchsia" decodeCommit)
        (Decode.field "rustfmt" decodeVersion)


decodeCommit : Decode.Decoder Commit
decodeCommit =
    Decode.map2 Commit
        (Decode.field "hash" Decode.string)
        (Decode.field "timestamp" decodeTimestamp)


decodeTimestamp : Decode.Decoder Time.Posix
decodeTimestamp =
    Decode.map (\t -> Time.millisToPosix (t * 1000)) Decode.int


decodeVersion : Decode.Decoder Version
decodeVersion =
    Decode.map Version
        (Decode.field "version" Decode.string)
