import * as esbuild from "esbuild";
import ElmPlugin from "esbuild-plugin-elm";
import CopyPlugin from 'esbuild-plugin-copy';
import { htmlPlugin } from "@craftamap/esbuild-plugin-html";

const prod = process.env.NODE_ENV === "production";

const ctx = await esbuild.context({
  entryPoints: ["src/main.ts"],
  bundle: true,
  metafile: true,
  minify: prod,
  sourcemap: !prod,
  outdir: "dist",
  define: {
    FIDLBOLT_BASE_URL: JSON.stringify(prod ? 'go/fidlbolt' : null),
  },
  plugins: [
    htmlPlugin({
      files: [
        {
          entryPoints: ["src/main.ts"],
          filename: "index.html",
          htmlTemplate: "src/index.html",
          favicon: "src/favicon.ico",
          hash: true,
        }
      ],
    }),
    ElmPlugin({
      cwd: "src/elm",
      debug: !prod,
      optimize: prod,
      verbose: true,
    }),
    CopyPlugin({
      once: true,
      assets: [
        {
          from: [
            './node_modules/ace-builds/src-min-noconflict/{theme,keybinding}-*.js'
          ],
          // Corresponds to basePath in src/ace-modes.js.
          to: ['./ace/'],
        },
      ],
    })
  ],
});

if (process.argv.includes("--watch")) {
  await ctx.watch();
} else {
  await ctx.rebuild();
  await ctx.dispose();
}
