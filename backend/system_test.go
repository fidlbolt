// Copyright 2020 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

//go:build linux || darwin

package main

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"testing"
	"time"
)

func TestTempDir(t *testing.T) {
	d, err := newTempDir()
	if err != nil {
		t.Fatal("failed to create temp dir: ", err)
	}
	defer os.RemoveAll(d.path)
	written := "the contents of foo"
	filename, err := d.createFile("foo", written)
	if err != nil {
		t.Fatal("failed to create temp file: ", err)
	}
	expectedFilename := d.join("foo")
	if filename != expectedFilename {
		t.Errorf("got %q, want %q", filename, expectedFilename)
	}
	read, err := d.readFile("foo")
	if err != nil {
		t.Fatal("failed to read temp file: ", err)
	}
	if read != written {
		t.Errorf("got %q, want %q", read, written)
	}
}

func TestFindFile(t *testing.T) {
	d1, err := newTempDir()
	if err != nil {
		t.Fatal("failed to create temp dir: ", err)
	}
	defer os.RemoveAll(d1.path)
	d2, err := newTempDir()
	if err != nil {
		t.Fatal("failed to create temp dir: ", err)
	}
	defer os.RemoveAll(d2.path)

	d1a, err := d1.createFile("a", "stuff")
	if err != nil {
		t.Fatal("failed to create temp file: ", err)
	}
	d2a, err := d2.createFile("a", "stuff")
	if err != nil {
		t.Fatal("failed to create temp file: ", err)
	}
	d2b, err := d2.createFile("b", "stuff")
	if err != nil {
		t.Fatal("failed to create temp file: ", err)
	}

	findD1a, err := findFile("a", []string{d1.path, d2.path})
	if err != nil {
		t.Fatal("failed to find d1/a: ", err)
	}
	if findD1a != d1a {
		t.Errorf("got %q, want %q", findD1a, d1a)
	}

	findD2a, err := findFile("a", []string{d2.path, d1.path})
	if err != nil {
		t.Fatal("failed to find d2/a: ", err)
	}
	if findD2a != d2a {
		t.Errorf("got %q, want %q", findD2a, d2a)
	}

	findD2b, err := findFile("b", []string{d1.path, d2.path})
	if err != nil {
		t.Fatal("failed to find d2/b: ", err)
	}
	if findD2b != d2b {
		t.Errorf("got %q, want %q", findD2b, d2b)
	}
}

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

var nonexistentPath = fmt.Sprintf("/does_not_exist.%020d", rand.Uint64())

func TestProgramRunSuccess(t *testing.T) {
	p := program{"/bin/echo"}
	ctx := context.Background()
	run, err := p.run(ctx, "hi")
	if err != nil {
		t.Fatal("failed to run echo: ", err)
	}
	if !run.success {
		t.Error("echo did not succeed")
	}
	stdout := "hi\n"
	stderr := ""
	if run.stdout != stdout {
		t.Errorf("got %q, want %q", run.stdout, stdout)
	}
	if run.stderr != stderr {
		t.Errorf("got %q, want %q", run.stderr, stderr)
	}
}

func TestProgramRunFailure(t *testing.T) {
	p := program{"/bin/cat"}
	ctx := context.Background()
	run, err := p.run(ctx, nonexistentPath)
	if err != nil {
		t.Fatalf("%s %s: %s", p.path, nonexistentPath, err)
	}
	if run.success {
		t.Errorf("%s %s: run.success: got %v, want false", p.path, nonexistentPath, run.success)
	}
	if run.stdout != "" {
		t.Errorf("%s %s: run.stdout: got %q, want %q", p.path, nonexistentPath, run.stdout, "")
	}
	if run.stderr == "" {
		t.Errorf("%s %s: run.stderr: got %q, want nonempty", p.path, nonexistentPath, run.stderr)
	}
}

func TestProgramRunNoLaunch(t *testing.T) {
	p := program{nonexistentPath}
	ctx := context.Background()
	_, err := p.run(ctx)
	if err == nil {
		t.Fatalf("%s: expected error", p.path)
	}
}

func TestProgramRunInfallibleSuccess(t *testing.T) {
	p := program{"/bin/echo"}
	ctx := context.Background()
	run, err := p.runInfallible(ctx, "hi")
	if err != nil {
		t.Fatal("failed to run echo: ", err)
	}
	if !run.success {
		t.Error("echo did not succeed")
	}
	stdout := "hi\n"
	stderr := ""
	if run.stdout != stdout {
		t.Errorf("got %q, want %q", run.stdout, stdout)
	}
	if run.stderr != stderr {
		t.Errorf("got %q, want %q", run.stderr, stderr)
	}
}

func TestProgramRunInfallibleFailure(t *testing.T) {
	p := program{"/bin/cat"}
	ctx := context.Background()
	_, err := p.runInfallible(ctx, nonexistentPath)
	if err == nil {
		t.Fatalf("%s %s: expected error", p.path, nonexistentPath)
	}
}
