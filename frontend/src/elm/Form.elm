-- Copyright 2020 The Fuchsia Authors. All rights reserved.
-- Use of this source code is governed by a BSD-style license that can be
-- found in the LICENSE file.


module Form exposing
    ( Button
    , Form
    , Identifier
    , Model
    , Msg
    , checkbox
    , conform
    , decode
    , decodeWithoutConforming
    , define
    , empty
    , encode
    , encodeEffective
    , encodeEffectiveCombined
    , getStringValue
    , groupedSelect
    , insertStringValue
    , nonnegative
    , number
    , positive
    , select
    , text
    , update
    , view
    )

{-| This module provides abstractions for forms.

There are three main types:

  - Input: Declarative representation of an HTML form input.
  - Model: Concrete assignment of values to each form input.
  - Form: List of Inputs together with a default Model.

The rendered form looks like this:

    <form class="form">
        <div class="form-control">
            <label>
                The label:
                <input ... >
            </label>
            ...
        </div>
        ...
    </form>

There are five kind of inputs:

  - Checkbox:
    <input type="checkbox" class="form-checkbox">
  - Text:
    <input type="text" class="form-text">
  - Number:
    <input type="number" class="form-number">
  - Select:
    <select class="form-select"><option class="form-option"> ... </select>

Forms can also have buttons at the end:

    <button class="form-button"> ... </button>

-}

import Dict exposing (Dict)
import Html exposing (Html)
import Html.Attributes as Attributes
import Html.Events as Events
import Json.Decode as Decode
import Json.Encode as Encode



------------- INPUT ------------------------------------------------------------


type alias Input =
    { id : Identifier
    , label : String
    , control : Control
    }


type alias Identifier =
    String


type Control
    = Checkbox
    | Text TextConfig
    | Number NumberConfig
    | Select OptionList


type alias TextConfig =
    { validate : String -> Bool
    , placeholder : Maybe String
    , invalidMessage : String
    }


type alias NumberConfig =
    { validate : Int -> Bool }


type OptionList
    = Flat (List Option)
    | Grouped (List ( String, List Option ))


type alias Option =
    { id : Identifier
    , label : String
    }



------------- MODEL ------------------------------------------------------------


type alias Model =
    Dict Identifier Value


type Value
    = BoolValue Bool
    | ValidatedStringValue ValidatedString
    | ValidatedIntValue ValidatedInt


type alias ValidatedString =
    { -- The raw user input, possibly invalid.
      raw : String

    -- True if the raw input passed validation.
    , valid : Bool

    -- The most recent valid input.
    , string : String
    }


type alias ValidatedInt =
    { -- The raw user input, possibly invalid.
      raw : String

    -- True if the raw input passed validation.
    , valid : Bool

    -- The most recent valid input.
    , int : Int
    }


init : List ( Identifier, Value ) -> Model
init entries =
    Dict.fromList entries


{-| Form models should usually start out empty. This means the default values
will be used, and nothing will be stored in local storage until the user
explicitly makes choices.
-}
empty : Model
empty =
    Dict.empty


insertStringValue : Identifier -> String -> Model -> Model
insertStringValue id string model =
    Dict.insert id (ValidatedStringValue (fromString string)) model


effective :
    (Identifier -> Model -> Maybe a)
    -> a
    -> Form
    -> Identifier
    -> Model
    -> a
effective get fallbackValue form id model =
    case get id model of
        Just value ->
            value

        Nothing ->
            case get id form.default of
                Just value ->
                    value

                Nothing ->
                    fallbackValue


getBoolValue : Form -> Identifier -> Model -> Bool
getBoolValue =
    let
        get id model =
            case Dict.get id model of
                Just (BoolValue bool) ->
                    Just bool

                _ ->
                    Nothing
    in
    effective get False


getValidatedStringValue : Form -> Identifier -> Model -> ValidatedString
getValidatedStringValue =
    let
        get id model =
            case Dict.get id model of
                Just (ValidatedStringValue validatedString) ->
                    Just validatedString

                _ ->
                    Nothing
    in
    effective get { raw = "", valid = False, string = "" }


getStringValue : Form -> Identifier -> Model -> String
getStringValue form id model =
    (getValidatedStringValue form id model).string


getValidatedIntValue : Form -> Identifier -> Model -> ValidatedInt
getValidatedIntValue =
    let
        get id model =
            case Dict.get id model of
                Just (ValidatedIntValue validatedInt) ->
                    Just validatedInt

                _ ->
                    Nothing
    in
    effective get { raw = "", valid = False, int = 0 }


validateNewString : (String -> Bool) -> ValidatedString -> String -> ValidatedString
validateNewString validate previous string =
    let
        invalid =
            { raw = string, valid = False, string = previous.string }
    in
    if validate string then
        fromString string

    else
        invalid


validateNewInt : (Int -> Bool) -> ValidatedInt -> String -> ValidatedInt
validateNewInt validate previous string =
    let
        invalid =
            { raw = string, valid = False, int = previous.int }
    in
    case String.toInt string of
        Just int ->
            if validate int then
                fromInt int

            else
                invalid

        Nothing ->
            invalid


fromString : String -> ValidatedString
fromString string =
    { raw = string, valid = True, string = string }


fromInt : Int -> ValidatedInt
fromInt int =
    { raw = String.fromInt int, valid = True, int = int }


positive : NumberConfig
positive =
    { validate = \n -> n > 0 }


nonnegative : NumberConfig
nonnegative =
    { validate = \n -> n >= 0 }



------------- DEFINITION -------------------------------------------------------


type alias Form =
    { inputs : List Input
    , default : Model
    }


type alias Entry =
    { input : Input
    , default : Value
    }


{-| Defines a form with default values. This is the main entry point of this
module. Example usage:

    define
        [ text "productId" "Product ID" ""
        , number "quantity" "Quantity" 1
        , checkbox "gift" "Gift" False
        , select
            "method"
            "Method"
            [ ( "slow", "Slow" ), ( "fast", "Fast" ) ]
            "slow"
        ]

This creates a form with text, number, checkbox, and select inputs. Each helper
function takes an identifier and a user-facing label as its first two arguments.
The defaults in this case are an empty string, 1, False, and the "slow" option.

-}
define : List Entry -> Form
define entries =
    let
        idAndDefault entry =
            ( entry.input.id, entry.default )
    in
    { inputs = List.map .input entries
    , default = init (List.map idAndDefault entries)
    }


checkbox : Identifier -> String -> Bool -> Entry
checkbox =
    makeEntry Checkbox BoolValue


text : Identifier -> String -> String -> TextConfig -> Entry
text id label default config =
    makeEntry (Text config) (ValidatedStringValue << fromString) id label default


number : Identifier -> String -> Int -> NumberConfig -> Entry
number id label default config =
    makeEntry (Number config) (ValidatedIntValue << fromInt) id label default


makeEntry : Control -> (a -> Value) -> Identifier -> String -> a -> Entry
makeEntry control valueVariant id label default =
    { input =
        { id = id
        , label = label
        , control = control
        }
    , default = valueVariant default
    }


select :
    Identifier
    -> String
    -> List ( Identifier, String )
    -> Identifier
    -> Entry
select id label options default =
    let
        convert ( optionId, optionLabel ) =
            { id = optionId, label = optionLabel }
    in
    { input =
        { id = id
        , label = label
        , control = Select (Flat (List.map convert options))
        }
    , default = ValidatedStringValue (fromString default)
    }


groupedSelect :
    Identifier
    -> String
    -> List ( Identifier, List ( Identifier, String ) )
    -> Identifier
    -> Entry
groupedSelect id label groupedOptions default =
    let
        convert ( optionId, optionLabel ) =
            { id = optionId, label = optionLabel }

        convertGroup ( groupLabel, options ) =
            ( groupLabel, List.map convert options )
    in
    { input =
        { id = id
        , label = label
        , control = Select (Grouped (List.map convertGroup groupedOptions))
        }
    , default = ValidatedStringValue (fromString default)
    }


{-| Conforms a model to a form by removing invalid values.
-}
conform : Form -> Model -> Model
conform form model =
    let
        result =
            Dict.foldl conformValue model model

        conformValue id value accModel =
            if keepValue id value then
                accModel

            else
                Dict.remove id accModel

        keepValue id value =
            case
                form.inputs
                    |> List.filter (\input -> input.id == id)
                    |> List.head
            of
                Just input ->
                    keepValueForInput value input

                Nothing ->
                    False

        keepValueForInput value input =
            case input.control of
                Checkbox ->
                    case value of
                        BoolValue _ ->
                            True

                        _ ->
                            False

                Text _ ->
                    case value of
                        ValidatedStringValue _ ->
                            True

                        _ ->
                            False

                Number { validate } ->
                    case value of
                        ValidatedIntValue { int } ->
                            if validate int then
                                True

                            else
                                False

                        _ ->
                            False

                Select (Flat options) ->
                    keepValueForOptions value options

                Select (Grouped groupedOptions) ->
                    keepValueForOptions value
                        (List.concatMap Tuple.second groupedOptions)

        keepValueForOptions value options =
            case value of
                ValidatedStringValue chosenId ->
                    List.any (\{ id } -> id == chosenId.string) options

                _ ->
                    False
    in
    result



------------- UPDATE -----------------------------------------------------------


type Msg
    = NoOp
    | Update Identifier Value


update : Msg -> Model -> Model
update msg model =
    case msg of
        NoOp ->
            model

        Update id value ->
            Dict.insert id value model



------------- VIEW -------------------------------------------------------------


type alias Button msg =
    ( String, msg )


{-| Renders the form. Example usage:

    type alias MyMsg = FormMsg Form.Msg | DoSomething

    view form [ ( "Do Something", DoSomething ) ] FormMsg model

This renders a form with a button that performs DoSomething. There is no
onSubmit handler: instead, handle every field update immediately by intercepting
the Form.Update message.

-}
view : Form -> List (Button msg) -> (Msg -> msg) -> Model -> Html msg
view form buttons toMsg model =
    let
        inputViews =
            List.map (inputView toMsg form model) form.inputs

        buttonViews =
            List.map buttonView buttons

        buttonPart =
            case buttonViews of
                [] ->
                    []

                views ->
                    [ Html.div
                        [ Attributes.class "form-button-group" ]
                        views
                    ]
    in
    Html.form
        [ Attributes.class "form"

        -- Use an explicit onSubmit just to get the preventDefault behavior,
        -- stopping the browser from reloading with a query when you submit.
        , Events.onSubmit (toMsg NoOp)
        ]
        (inputViews ++ buttonPart)


inputView : (Msg -> msg) -> Form -> Model -> Input -> Html msg
inputView toMsg form model input =
    let
        set variant value =
            toMsg (Update input.id (variant value))

        content =
            case input.control of
                Checkbox ->
                    let
                        checked =
                            getBoolValue form input.id model
                    in
                    [ Html.input
                        [ Attributes.type_ "checkbox"
                        , Attributes.checked checked
                        , Attributes.class "form-checkbox"

                        -- Explicitly toggle the model value instead of using
                        -- the event parameter because of this bug:
                        -- https://github.com/elm/html/issues/188
                        , Events.onCheck (\_ -> set BoolValue (not checked))
                        ]
                        []
                    ]

                Text { validate, placeholder, invalidMessage } ->
                    let
                        value =
                            getValidatedStringValue form input.id model

                        change =
                            validateNewString validate value

                        placeholderAttribute =
                            case placeholder of
                                Just s ->
                                    [ Attributes.placeholder s ]

                                Nothing ->
                                    []

                        invalidMessageSpan =
                            if value.valid then
                                []

                            else
                                [ Html.span
                                    [ Attributes.class "form-invalid-message" ]
                                    [ Html.text invalidMessage ]
                                ]
                    in
                    Html.input
                        ([ Attributes.type_ "text"
                         , Attributes.value value.raw
                         , Attributes.classList
                            [ ( "form-text", True )
                            , ( "form-text--invalid", not value.valid )
                            ]
                         , Events.onInput (set (ValidatedStringValue << change))
                         ]
                            ++ placeholderAttribute
                        )
                        []
                        :: invalidMessageSpan

                Number { validate } ->
                    let
                        value =
                            getValidatedIntValue form input.id model

                        change =
                            validateNewInt validate value
                    in
                    [ Html.input
                        [ Attributes.type_ "number"
                        , Attributes.value value.raw
                        , Attributes.classList
                            [ ( "form-number", True )
                            , ( "form-number--invalid", not value.valid )
                            ]
                        , Events.onInput
                            (set (ValidatedIntValue << change))
                        ]
                        []
                    ]

                Select options ->
                    let
                        chosenId =
                            getStringValue form input.id model

                        optionView option =
                            Html.option
                                [ Attributes.value option.id
                                , Attributes.selected (option.id == chosenId)
                                ]
                                [ Html.text option.label ]

                        groupView ( groupLabel, groupOptions ) =
                            Html.optgroup
                                [ Attributes.attribute "label" groupLabel ]
                                (List.map optionView groupOptions)
                    in
                    [ Html.select
                        [ Attributes.class "form-select"
                        , Events.onInput (set (ValidatedStringValue << fromString))
                        ]
                        (case options of
                            Flat list ->
                                List.map optionView list

                            Grouped groups ->
                                List.map groupView groups
                        )
                    ]
    in
    Html.div
        [ Attributes.class "form-control" ]
        [ case input.control of
            Checkbox ->
                Html.label [] (content ++ [ Html.text input.label ])

            _ ->
                Html.label [] (Html.text input.label :: content)
        ]


buttonView : Button msg -> Html msg
buttonView ( title, msg ) =
    -- It's important to use <button type="button"> because the default button
    -- type is "submit" and submitting the form runs that button's onclick
    -- handler, which we don't want.
    Html.button
        [ Attributes.type_ "button"
        , Attributes.class "button form-button"
        , Events.custom "click"
            (Decode.succeed
                { message = msg
                , stopPropagation = True
                , preventDefault = True
                }
            )
        ]
        [ Html.text title ]



------------- ENCODE / DECODE --------------------------------------------------


{-| Encodes only the form values that have been explicitly set by the user. This
should be used for persisting values to the browser.
-}
encode : Model -> Encode.Value
encode model =
    Encode.dict identity encodeValue model


{-| Encodes the effective values for the form. Unlike the encode function, this
includes default values for inputs that have not been explicitly set by the
user. This is useful for communicating to other parts of the system that need to
use the effective values.
-}
encodeEffective : Form -> Model -> Encode.Value
encodeEffective form model =
    Dict.union model form.default |> encode


{-| Like encodeEffective but takes multiple forms and combines them.
-}
encodeEffectiveCombined : List ( Form, Model ) -> Encode.Value
encodeEffectiveCombined items =
    let
        multiUnion =
            List.foldl Dict.union Dict.empty

        values =
            multiUnion (List.map (\( _, model ) -> model) items)

        defaults =
            multiUnion (List.map (\( form, _ ) -> form.default) items)
    in
    Dict.union values defaults |> encode


encodeValue : Value -> Encode.Value
encodeValue value =
    case value of
        BoolValue bool ->
            Encode.bool bool

        ValidatedStringValue { string } ->
            Encode.string string

        ValidatedIntValue { int } ->
            Encode.int int


decode : Form -> Decode.Decoder Model
decode form =
    decodeWithoutConforming |> Decode.map (conform form)


decodeWithoutConforming : Decode.Decoder Model
decodeWithoutConforming =
    Decode.dict decodeValue


decodeValue : Decode.Decoder Value
decodeValue =
    Decode.oneOf
        [ Decode.map BoolValue Decode.bool
        , Decode.map (ValidatedStringValue << fromString) Decode.string
        , Decode.map (ValidatedIntValue << fromInt) Decode.int
        , Decode.fail "Invalid value type"
        ]
