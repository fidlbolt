-- Copyright 2020 The Fuchsia Authors. All rights reserved.
-- Use of this source code is governed by a BSD-style license that can be
-- found in the LICENSE file.


module Share exposing
    ( Model
    , Msg
    , init
    , subscriptions
    , update
    , view
    )

{-| This module produces a view containing a share link.
-}

import Html exposing (Html)
import Html.Attributes as Attributes
import Json.Decode as Decode
import Ports



------------- MODEL ------------------------------------------------------------


type alias Model =
    { url : Maybe String
    , copied : Bool
    }


init : Model
init =
    { url = Nothing
    , copied = False
    }



------------- UPDATE -----------------------------------------------------------


type Msg
    = NoOp
    | SetUrl String
    | SetCopied


update : Msg -> Model -> ( Model, Cmd msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        SetUrl url ->
            ( { model | url = Just url, copied = False }
            , Ports.selectAndCopyShareLink
            )

        SetCopied ->
            ( { model | copied = True }
            , Cmd.none
            )



------------- SUBSCRIPTIONS ----------------------------------------------------


subscriptions : Sub Msg
subscriptions =
    Sub.batch
        [ Ports.shareLinkUpdated NoOp (Decode.map SetUrl Decode.string)
        , Ports.shareLinkCopied NoOp (Decode.succeed SetCopied)
        ]



------------- VIEW -------------------------------------------------------------


view : Model -> Html msg
view model =
    let
        multiline =
            Html.text << String.trim

        copiedNotice =
            if model.copied then
                [ Html.span
                    [ Attributes.class "share-copied" ]
                    [ Html.text "Copied!" ]
                ]

            else
                []
    in
    Html.div []
        ([ Html.p []
            [ Html.text "Use the link below to share this fidlbolt!" ]
         , Html.p []
            [ multiline """
Please do not share it in public bugs, CLs, etc. since fidlbolt is not yet
ready for external use.
"""
            ]
         , Html.input
            [ Attributes.type_ "text"
            , Attributes.id "ShareLink"
            , Attributes.class "share-url"
            , Attributes.value (Maybe.withDefault "" model.url)
            ]
            []
         ]
            ++ copiedNotice
        )
