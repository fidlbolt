// Copyright 2021 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package main

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestParseFidlGood(t *testing.T) {
	tests := []struct {
		input    string
		expected parsedFidl
	}{
		{
			input: `library x;`,
			expected: parsedFidl{
				Decl: parsedLibrary{"x", 1, 9},
			},
		},
		{
			input: `library foo.bar.baz;`,
			expected: parsedFidl{
				Decl: parsedLibrary{"foo.bar.baz", 1, 9},
			},
		},
		{
			// Note: fidlc rejects this, but it's not worth the effort here.
			input: `library Foo_Bar;`,
			expected: parsedFidl{
				Decl: parsedLibrary{"Foo_Bar", 1, 9},
			},
		},
		{
			// Note: confirmed that fidlc accepts this.
			input: ` library  foo . bar ; `,
			expected: parsedFidl{
				Decl: parsedLibrary{"foo.bar", 1, 11},
			},
		},
		{
			input: `@attr library foo.bar;`,
			expected: parsedFidl{
				Decl: parsedLibrary{"foo.bar", 1, 15},
			},
		},
		{
			input: `@attr(1) library foo.bar;`,
			expected: parsedFidl{
				Decl: parsedLibrary{"foo.bar", 1, 18},
			},
		},
		{
			input: `@attr(name=value) library foo.bar;`,
			expected: parsedFidl{
				Decl: parsedLibrary{"foo.bar", 1, 27},
			},
		},
		{
			input: `library foo.bar; const FOO int32 = 1 | 2;`,
			expected: parsedFidl{
				Decl: parsedLibrary{"foo.bar", 1, 9},
			},
		},
		{
			input: `library foo.bar; We ignore the rest of the file!`,
			expected: parsedFidl{
				Decl: parsedLibrary{"foo.bar", 1, 9},
			},
		},
		{
			input: `
// Some comments
library
// Some comments
foo.bar;
			`,
			expected: parsedFidl{
				Decl: parsedLibrary{"foo.bar", 5, 1},
			},
		},
		{
			input: `
library foo;
using bar;
using baz.qux;
`,
			expected: parsedFidl{
				Decl: parsedLibrary{"foo", 2, 9},
				Using: []parsedLibrary{
					{"bar", 3, 7},
					{"baz.qux", 4, 7},
				},
			},
		},
		{
			input: `
// library foo;
@attr("library foo")
@attr("\\\"")
@attr (
	a="using a;",
	// using b;
	c="using c;")
library foo;
// using d;
using
//
first; using second ;
We ignore the rest of the file.
Even if it has 𝔰𝔱𝔯𝔞𝔫𝔤𝔢 characters.
`,
			expected: parsedFidl{
				Decl: parsedLibrary{"foo", 9, 9},
				Using: []parsedLibrary{
					{"first", 13, 1},
					{"second", 13, 14},
				},
			},
		},
	}
	for _, tc := range tests {
		actual, err := parseFidl(tc.input)
		if err != nil {
			t.Errorf("input:\n%s\n\nfailed: %s", tc.input, err)
		} else if diff := cmp.Diff(tc.expected, actual); diff != "" {
			t.Errorf("input:\n%s\n\ndiff (-want +got):\n%s", tc.input, diff)
		}
	}
}

func TestParseFidlBad(t *testing.T) {
	tests := []string{
		``,
		`library`,
		`library foo`,
		`using bar;`,
		`using bar; library foo;`,
		`// library foo;`,
	}
	for _, input := range tests {
		actual, err := parseFidl(input)
		if err == nil {
			t.Errorf("input:\n%s\n\nunexpectedly succeeded. output:\n%+v", input, actual)
		}
	}
}
