module fidlbolt

go 1.23

require (
	github.com/alecthomas/participle/v2 v2.1.1
	github.com/google/go-cmp v0.6.0
)
