# fidlbolt

fidlbolt is web app for exploring FIDL code and bytes, inspired by Matt Godbolt's [Compiler Explorer][godbolt].

## Get started

To run a local fidlbolt server:

1. Follow the [Fuchsia: Get Started][fuchsia] guide.
2. Build FIDL tools: `fx build host_x64/{fidlc,fidlgen_{cpp,hlcpp,rust,go},fidl-{lint,format}}`.
3. Ensure that you have [Go][] and [Node.js][] installed.
4. Build and run with `make && make run`.
5. View the app at `http://localhost:8080`.

If you don't want changes in the Fuchsia tree (e.g. rebuilding fidlc or changing FIDL libraries) to affect the running fidlbolt, run `./prepare_deployment.sh` and use `make run DEPLOYMENT=1` instead.

## Contributing

### Backend

The backend is written in Go. It uses [net/http][] to serve static files and handle POST requests.

### Frontend

The frontend is written in [Elm][] and [TypeScript][]. It uses [esbuild][] for bundling.

To set up the frontend:

1. Install [Node.js][] and [npm][]. On Debian-based systems, use `sudo apt-get install npm`.
2. `cd frontend && npm ci` (using `ci` instead of `install` ensures a repeatable build).

Then, use one of the commands listed in `npm run`:

- `npm run dev`: build in development mode
- `npm run watch`: rebuild in development mode on every file change
- `npm run tc`: typecheck TypeScript files
- `npm run lint`: run eslint on TypeScript files
- `npm run fix`: format and fix lint issues in TypeScript and Elm files
- `npm run build`: build in production mode

The build commands read files in `frontend/src/` and generate output in `frontend/dist/`.

### Workflow

If you are developing on Linux, `./watch.sh` provides an easy workflow. It automatically watches code in the frontend (using `npm run watch`) and backend (using inotifywait), rebuilding and restarting the server as necessary. It does not perform hot/live reloading: you still need to refresh the browser manually to see changes.

### Style

Before making a CL, always run `make format`.

## Deployment

The project uses [Docker][] for containerized deployment.

To build a new image:

1. Install [Docker][]. On Debian-based systems, use `sudo apt-get install docker-ce`.
2. `./prepare_deployment.sh`
3. `sudo docker image build -t fidlbolt .`

To run the image in a container locally:

1. `sudo docker container run --publish 8080:8080 --detach --name fb fidlbolt`
2. (When you're done) `sudo docker container rm fb`

To push the image to the [GCP Artifact Registry][artifact-registry]:

1. (Only the first time) `sudo gcloud auth configure-docker us-central1-docker.pkg.dev`
2. `sudo docker tag fidlbolt us-central1-docker.pkg.dev/PROJECT_ID/REPOSITORY/IMAGE`
3. `sudo docker push us-central1-docker.pkg.dev/PROJECT_ID/REPOSITORY/IMAGE`

[Docker]: https://www.docker.com/
[Elm]: https://elm-lang.org/
[Go]: https://golang.org/
[Node.js]: https://nodejs.org/
[npm]: https://www.npmjs.com/get-npm
[TypeScript]: https://www.typescriptlang.org/
[fuchsia]: https://fuchsia.dev/fuchsia-src/getting_started
[godbolt]: https://godbolt.org/
[net/http]: https://golang.org/pkg/net/http/
[esbuild]: https://esbuild.github.io/
[artifact-registry]: https://cloud.google.com/artifact-registry/docs/docker/pushing-and-pulling
