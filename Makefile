# Flags to fidlbolt server.
PORT ?= 8080
VERBOSE ?= 0

# If 0, use files directly from $FUCHSIA_DIR.
# If 1, use files from ./deployment (see ./prepare_deployment.sh).
DEPLOYMENT ?= 0

# Detect host platform.
uname_s := $(shell uname -s)
ifeq ($(uname_s),Linux)
host_platform := linux-x64
else ifeq ($(uname_s),Darwin)
host_platform := mac-x64
else
$(error Unsupported platform $(uname_s))
endif

# Set paths to external resources used by the server.
# Note: the $\ ending used below breaks lines without inserting spaces:
# https://www.gnu.org/software/make/manual/make.html#Splitting-Lines
ifeq ($(DEPLOYMENT),1)
ifeq ($(wildcard deployment),)
$(error "No ./deployment directory found. Run ./prepare_deployment.sh")
endif
bin := ./deployment/bin
etc := ./deployment/etc
# TODO(fxbug.dev/72629) Remove banjo.
fidl := ./deployment/fuchsia/sdk/fidl$\
	:./deployment/fuchsia/sdk/banjo$\
	:./deployment/fuchsia/zircon
else
build_dir := $(file < $(FUCHSIA_DIR)/.fx-build-dir)
# The path in .fx-build-dir can be relative to $FUCHSIA_DIR or absolute. If it's
# relative, erasing /% is a no-op, and the ternary evaluates to $(FUCHSIA_DIR)/.
# If it's absolute, erasing /% yields an empty string (false), and the ternary
# evaluates to an empty string.
build_dir := $(if $(patsubst /%,,$(build_dir)),$(FUCHSIA_DIR)/,)$(build_dir)
bin := $(build_dir)/host_x64$\
	:$(FUCHSIA_DIR)/prebuilt/third_party/clang/$(host_platform)/bin$\
	:$(FUCHSIA_DIR)/prebuilt/third_party/rust/$(host_platform)/bin$\
	$(if $(FIDLBOLT_PATH),:$(FIDLBOLT_PATH),)
etc := $(FUCHSIA_DIR)
# TODO(fxbug.dev/72629) Remove banjo.
fidl := $(FUCHSIA_DIR)/sdk/fidl$\
	:$(FUCHSIA_DIR)/sdk/banjo$\
	:$(FUCHSIA_DIR)/zircon
endif

# Path to the server binary.
server := backend/dist/fidlbolt

.PHONY: all help frontend backend test run format clean

all: frontend backend

help:
	@echo "Targets:"
	@echo "    all        Build everything."
	@echo "    help       Show this help message."
	@echo "    frontend   Build the frontend/dist bundle using webpack."
	@echo "               For more build options: cd frontend && npm run."
	@echo "    backend    Build the $(server) server binary."
	@echo "    test       Build and run all tests."
	@echo "    run        Run the fidlbolt server on \$$PORT."
	@echo "               Set VERBOSE=1 to enable verbose logging."
	@echo "               Set DEPLOYMENT=1 to use files in ./deployment instead"
	@echo "               of \$$FUCHSIA_DIR (see ./prepare_deployment.sh)."
	@echo "               Note: rebuilds targets only if they do not exist."
	@echo "               To ensure an up-to-date server: make && make run."
	@echo "    format     Auto-format all code."
	@echo "    clean      Remove all generated files."

frontend: frontend/dist

frontend/node_modules: frontend/package*.json
	cd frontend && npm ci

frontend/dist: frontend/node_modules frontend/src/* frontend/src/elm/*
	cd frontend && npm run build

backend: $(server)

$(server): backend/go.mod backend/*.go
	cd backend && go build -o ../$@

test:
	cd backend && go test -v

run: $(if $(wildcard frontend/dist),,frontend)
run: $(if $(wildcard $(server)),,backend)
ifndef FUCHSIA_DIR
	$(error FUCHSIA_DIR is not defined. Either pass in FUCHSIA_DIR="path" or define the variable)
endif
	./$(server) \
		-static=frontend/dist \
		-bin=$(bin) \
		-etc=$(etc) \
		-fidl=$(fidl) \
		-port=$(PORT) \
		-verbose=$(VERBOSE)

format:
	cd frontend && npm run fmt
	cd backend && go fmt

clean:
	rm -rf backend/dist/
	rm -rf frontend/dist/
	rm -rf deployment/
