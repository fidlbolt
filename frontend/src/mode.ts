// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import * as ace from './ace';

// A fidlbolt input/output mode. Corresponds to toString in elm/Mode.elm.
export type Mode =
  | 'bytes+'
  | 'bytes'
  | 'cpp'
  | 'dart'
  | 'diff'
  | 'fidl'
  | 'fidltext'
  | 'go'
  | 'hlcpp'
  | 'json'
  | 'rust';

export type InputMode = 'fidl' | 'fidltext' | 'bytes';

export type OutputMode = Mode;

export const DEFAULT_INPUT: Record<InputMode, string> = {
  fidl: `\
library test.fidlbolt;

// Type your code here!

protocol Calculator {
    Square(struct { x int64; }) -> (struct { result int64; });
};
`,

  fidltext: `\
FIDL Text is coming in FTP-058.
`,

  bytes: `\
// Type your bytes here!

00 01 ff FF a0b1 c2d3 // comments are ignored
8badf00d DEAD BEEF    // group bytes together
[0, 1, 2, 3, 4, 255]  // or list them in decimal
"Hello, World!\\n" 00  // strings work too
123u32 -1i8 0x80u64   // little endian integers
0o755u64 0b10101111u8 // cover all your bases!
`,
};

const ACE_MODES: Record<Mode, string> = {
  'bytes+': 'text',
  bytes: 'bytes',
  cpp: 'c_cpp',
  dart: 'dart',
  diff: 'text',
  fidl: 'fidl',
  fidltext: 'text',
  go: 'golang',
  hlcpp: 'c_cpp',
  json: 'json',
  rust: 'rust',
};

// Converts a fidlbolt mode to an Ace mode. Defaults to the text mode.
export function getAceMode(mode: Mode): ace.Mode {
  const aceMode = 'ace/mode/' + (ACE_MODES[mode] ?? 'text');
  return aceMode as unknown as ace.Mode;
}
