-- Copyright 2021 The Fuchsia Authors. All rights reserved.
-- Use of this source code is governed by a BSD-style license that can be
-- found in the LICENSE file.


module News exposing
    ( Model
    , Msg
    , decode
    , encode
    , init
    , update
    , view
    )

import Html exposing (Html)
import Html.Attributes as Attributes
import Html.Events as Events
import Json.Decode as Decode
import Json.Encode as Encode
import Mode exposing (Mode(..))


{-| This module defines news items that display at the top of the page and can
be dismissed. Dismissal is remembered across sessions.
-}



------------- MODEL ------------------------------------------------------------


type alias Model =
    { show : List Item }


type Item
    = -- Example exists just so that there is always at least one item.
      Example


allItems : List Item
allItems =
    []


init : Model
init =
    { show = allItems }


{-| Returns all items not in the given list. We can't use the Set library
because it doesn't support custom types (Item) nor does it support extracting
the smallest element.
-}
complement : List Item -> List Item
complement items =
    List.filter (\x -> not (List.member x items)) allItems



------------- UPDATE -----------------------------------------------------------


type Msg
    = Dismiss


update : Msg -> Model -> Model
update _ model =
    { show =
        case model.show of
            _ :: rest ->
                rest

            [] ->
                []
    }



------------- VIEW -------------------------------------------------------------


{-| Creates the news view. The toMsg parameter converts a News.Msg to the
top-level Msg type of the app.
-}
view : (Msg -> msg) -> Model -> Html msg
view toMsg model =
    let
        dismissButton =
            Html.button
                [ Attributes.class "button dismiss-button"
                , Events.onClick (toMsg Dismiss)
                ]
                [ Html.text "dismiss" ]
    in
    case model.show of
        item :: _ ->
            Html.div
                [ Attributes.class "news" ]
                (itemView item ++ [ dismissButton ])

        [] ->
            Html.text ""


itemView : Item -> List (Html msg)
itemView item =
    case item of
        Example ->
            []



------------- ENCODE / DECODE --------------------------------------------------


encode : Model -> Encode.Value
encode model =
    Encode.object
        [ ( "dismissed", Encode.list encodeItem (complement model.show) ) ]


decode : Decode.Decoder Model
decode =
    Decode.field "dismissed"
        (Decode.list decodeItem
            |> Decode.map (List.filterMap identity)
            |> Decode.map complement
            |> Decode.map Model
        )


encodeItem : Item -> Encode.Value
encodeItem item =
    case item of
        Example ->
            Encode.string "example"


decodeItem : Decode.Decoder (Maybe Item)
decodeItem =
    Decode.string
        |> Decode.andThen
            (\string ->
                case string of
                    "example" ->
                        Decode.succeed (Just Example)

                    -- Ignore unrecognized (old) news items.
                    _ ->
                        Decode.succeed Nothing
            )
