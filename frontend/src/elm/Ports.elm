-- Copyright 2020 The Fuchsia Authors. All rights reserved.
-- Use of this source code is governed by a BSD-style license that can be
-- found in the LICENSE file.


port module Ports exposing
    ( applySettings
    , clearDataAndReload
    , deploymentUpdated
    , persistModel
    , resizeEditors
    , selectAndCopyShareLink
    , setMainTabEnabled
    , shareLinkCopied
    , shareLinkUpdated
    , updateEditors
    , updateShareLink
    )

{-| This module defines ports for communicating with JavaScript.
-}

import Json.Decode as Decode
import Json.Encode as Encode



------------- OUTGOING -----------------------------------------------------------


{-| Sends a value to JavaScript. This is the only outgoing port.
-}
port toJS : Encode.Value -> Cmd msg


command : String -> Encode.Value -> Cmd msg
command cmd value =
    toJS <|
        Encode.object
            [ ( "command", Encode.string cmd )
            , ( "payload", value )
            ]


persistModel : Encode.Value -> Cmd msg
persistModel model =
    command "persistModel" model


clearDataAndReload : Cmd msg
clearDataAndReload =
    command "clearDataAndReload" Encode.null


applySettings : Encode.Value -> Cmd msg
applySettings settings =
    command "applySettings" settings


setMainTabEnabled : Bool -> Cmd msg
setMainTabEnabled enabled =
    command "setMainTabEnabled" (Encode.bool enabled)


updateEditors : Encode.Value -> Encode.Value -> Encode.Value -> Cmd msg
updateEditors inputMode outputMode options =
    command "updateEditors" <|
        Encode.object
            [ ( "input", inputMode )
            , ( "output", outputMode )
            , ( "options", options )
            ]


resizeEditors : Cmd msg
resizeEditors =
    command "resizeEditors" Encode.null


updateShareLink : Cmd msg
updateShareLink =
    command "updateShareLink" Encode.null


selectAndCopyShareLink : Cmd msg
selectAndCopyShareLink =
    command "selectAndCopyShareLink" Encode.null



------------- INCOMING ---------------------------------------------------------


{-| Receives a value from JavaScript. This is the only input port.
-}
port fromJS : (Decode.Value -> msg) -> Sub msg


subscribe : String -> msg -> Decode.Decoder msg -> Sub msg
subscribe cmd noOp decodeMsg =
    let
        decode =
            Decode.field "command" Decode.string
                |> Decode.andThen
                    (\passedCmd ->
                        if passedCmd == cmd then
                            Decode.field "payload" decodeMsg

                        else
                            Decode.fail "Message is not for us"
                    )

        handler value =
            case value |> Decode.decodeValue decode of
                Err _ ->
                    noOp

                Ok message ->
                    message
    in
    fromJS handler


deploymentUpdated : msg -> Decode.Decoder msg -> Sub msg
deploymentUpdated =
    subscribe "deploymentUpdated"


shareLinkUpdated : msg -> Decode.Decoder msg -> Sub msg
shareLinkUpdated =
    subscribe "shareLinkUpdated"


shareLinkCopied : msg -> Decode.Decoder msg -> Sub msg
shareLinkCopied =
    subscribe "shareLinkCopied"
