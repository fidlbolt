// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import * as lz from 'lz-string';
import {Editors} from './editors';
import {Evaluator} from './evaluator';

// Set in webpack.config.js.
declare const FIDLBOLT_BASE_URL: string | null;

// Query parameter name. In the future there could be v2, v3, etc.
const V1 = 'v1';

// We add a prefix before compression so that we can make sure it's there after
// decomopression (lz-string does not report errors).
const PREFIX = 'fidl:';

function encode(fidl: string): URLSearchParams {
  const prefixed = `${PREFIX}${fidl}`;
  const encoded = lz.compressToEncodedURIComponent(prefixed);
  // Note: Not using the URLSearchParams.append API because that would re-encode
  // the value which lz-string has already URI-encoded.
  return new URLSearchParams(`?${V1}=${encoded}`);
}

function decode(params: URLSearchParams): string | undefined {
  const encoded = params.get(V1);
  if (encoded == undefined) {
    return undefined;
  }
  const prefixed = lz.decompressFromEncodedURIComponent(encoded);
  if (prefixed == undefined || prefixed == '') {
    console.error('failed to decompress query parameter');
    return undefined;
  }
  if (!prefixed.startsWith(PREFIX)) {
    console.error('invalid query parameter prefix');
    return undefined;
  }
  const fidl = prefixed.slice(PREFIX.length);
  return fidl;
}

export class LinkSharing {
  private fidl?: string;
  private editors?: Editors;
  private evaluator?: Evaluator;

  constructor() {
    this.fidl = decode(new URLSearchParams(window.location.search));
  }

  // Fixes the saved model before it gets passed to Elm.
  fixupModel(savedModel?: any): void {
    if (savedModel != undefined && this.fidl != undefined) {
      // Focus on the FIDL tab so that when we call
      // editors.inputEditor.setValue() it writes in the correct session.
      savedModel.editors.activeInput = 'fidl';
    }
  }

  // Initializes the editor from URL state.
  init(editors: Editors, evaluator: Evaluator): void {
    this.editors = editors;
    this.evaluator = evaluator;
    if (this.fidl == undefined) {
      return;
    }
    evaluator.disableSaving();
    const callback = () => {
      // Unlike editors.sessions.fidl.session.setValue(), calling setValue()
      // on the editor directly will preserve undo history.
      editors.inputEditor.setValue(this.fidl!);
      // After setting the value, it selects everything and scrolls to the
      // bottom, so undo that.
      editors.inputEditor.clearSelection();
      editors.inputEditor.moveCursorTo(0, 0);
      this.disableSavingUntilNextChange();
      editors.inputEditor.removeEventListener('changeSession', callback);
    };
    // Wait until the input editor's session is ready, otherwise setting the
    // value doesn't work.
    editors.inputEditor.on('changeSession', callback);
  }

  // Creates and returns new share link. Also updates the browser address bar
  // with the same query string so that refreshing will reload the same content.
  createShareLink(): string {
    this.fidl = this.editors!.sessions.fidl.session.getValue();
    const search = '?' + encode(this.fidl).toString();
    window.history.replaceState(null, '', search);
    this.disableSavingUntilNextChange();
    if (FIDLBOLT_BASE_URL) {
      return FIDLBOLT_BASE_URL + search;
    }
    return window.location.origin + search;
  }

  // Whenever the editor content corresponds to the URL (i.e. when opening a
  // share link, or right after creating one), we enter "share mode". In share
  // mode, refreshing loads content from the URL, not from local storage. But as
  // soon as the user changes that content, we go back to normal mode, updating
  // local storage and removing the query string from the URL.
  private disableSavingUntilNextChange() {
    this.evaluator!.disableSavingUntilNextChange(() => {
      window.history.replaceState(null, '', '/');
    });
  }
}
